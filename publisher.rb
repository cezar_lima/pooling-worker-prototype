require 'bunny'

conn = Bunny.new
conn.start

ch = conn.channel
x = ch.default_exchange

queues = []
10.times do |i|
  queues << ch. queue("queue-#{i}")
end
i = 0
loop do
  queues.each do |queue|
    x.publish("Message #{i}", routing_key: queue.name, expiration: 60_000)
  end
  i += 1
  sleep(1)
end
