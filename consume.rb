require 'bunny'
require 'byebug'
require 'json'
require 'logger'
require 'rainbow'
require 'rest-client'

STDOUT.sync = true

USER = 'guest'
PASSWORD = 'guest'
URL = 'localhost:15672/api/queues/'

# Configure worker
MAX_THREADS = 20
WORK_DELAY = 1

# Configure message consumer
MAX_MESSAGES = 1000
DEALAY_SECONDS = 10

$logger = Logger.new(STDOUT)

conn = Bunny.new
conn.start


# All threads are sharing the same channel since multi-message acknowledgements
# are not used. See: http://rubybunny.info/articles/concurrency.html

# Note: manual ACK must be performed on the same channel a message was pulled.
# If other channel tries to ACK/reject and delivery_tag does not exists in it,
# this channel is closed
ch = conn.create_channel

# Uses HTTP API from rabbitmq_management plugin to get queues list
def get_queues(channel)
  response = RestClient.get("http://#{USER}:#{PASSWORD}@#{URL}").body
  queues = JSON.load(response)
  queues.map { |queue| channel.queue(queue['name']) }
end

def do_work(queue)
  Thread.new do
    delivery_info, properties, payload = queue.pop(manual_ack: true)
    unless delivery_info.nil?
      if rand(0..9) == 0
        $logger.debug(Rainbow("rejected #{payload} by #{Thread.current}").red)
        queue.channel.reject(delivery_info.delivery_tag, true)
      else
        $logger.debug(Rainbow("consumed #{payload} by #{Thread.current}").green)
        queue.channel.ack(delivery_info.delivery_tag)
      end
      sleep WORK_DELAY
    end
  end
end

# Get messages from RabbitMQ queues
loop do
  queues = get_queues(ch)
  n = MAX_MESSAGES / queues.size

  # Pull n messages for each queue every 5 seconds
  n.times do
    queues.select { |queue| queue.message_count > 0 }.each do |queue|
      if Thread.list.size < MAX_THREADS
        do_work(queue)
      end
    end
  end
  sleep DEALAY_SECONDS
end

