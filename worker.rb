require 'bunny'
require 'logger'

$logger = Logger.new(STDOUT)

class PoolingWorker < Bunny::Consumer
  def initialize(*args)
    super
    @on_delivery = default_handler
  end

  def cancelled?
    @cancelled
  end

  def handle_cancellation(basic_cancel)
    super
    @cancelled = true
  end

  private
  def default_handler
    Proc.new do |delivery_info, metadata, payload|
      puts "#{payload} from queue => #{queue.name}"
      handle_cancellation(cancel)
    end
  end
end
