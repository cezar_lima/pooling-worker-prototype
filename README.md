# Pooling Prototype
A proptotype of Bunny/Sneakers worker/consumer that uses pooling to get messages
from RabbitMQ queues and allows to control how many messages are taken from each
one.

If possible implements rate limit for workers.

## Features

#### Max retries
* AMQP 0.9.1 has field delivery-count (optional), but this field is not
  implemented yet by RabbitMQ 3.7
  ([roadmap for 3.8](https://github.com/rabbitmq/rabbitmq-server/issues/502))

* RabbitMQ has [TTL](http://www.rabbitmq.com/ttl.html) for messages

  _Note: if a message without TTL is the first in a queue, the TTL messages
  are not dropped util it leaves que queue_

#### Ack/reject
* Use queue manual ack

  It requires that `Bunny::Channel#ack` and `Bunny::Channel#reject` uses same
  channel the message was retrieved

## Architecture

It is an loop Ruby process, that:

1. Asks RabbitMQ its queue list
2. Gets messages count for each queue
3. For each queue that is not empty, get _n_ messages from it and sends for an
  worker Thread

### Configuration

* Max number of worker Threads (`MAX_THREADS`)
* Priority for each queue (for tests use uniform value)


## Testing

##### Initiate the publisher

```ruby publisher.rb```

This publisher starts 10 queues (not durable) and sends 1 message/s for each of
tehm while it is runnig

##### Initiate consumers

```ruby consume.rb```

Begin cosuming the queues messages and print logs for each message consumed
(green for acked message and red for rejected/requeued)
